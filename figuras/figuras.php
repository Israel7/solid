<?php
Class Circle 
{
    public $radius;

    public function __construct($radius) 
    {
        $this->radius = $radius;
    }

    public function area()
    {
        return pi() * pow($this->radius, 2);
    }
}

Class Square 
{
    public $length;

    public function __construct($length) 
    {
        $this->length = $length;
    }

    public function area()
    {
        return pow($this->length, 2);
    }
}

Class Cube
{
    public $length;

    public function __construct($length)
    {
        $this->length = $length;
    }

    public function area()
    {
        return pow($this->length, 2) * 6;
    }

    public function volumen()
    {
        return pow($this->length, 3);
    }
}

class VolumeCalculator extends AreaCalculator
{
    public function __construct($shapes = array())
    {
        parent::__construct($shapes);
    }

    public function sum()
    {
        $summedData = '';
        return $summedData;
    }
}

class AreaCalculator
{
    protected $shapes;

    public function __construct($shapes = array())
    {
        $this->shapes = $shapes;
    }
    
    public function sum()
    {
        foreach ($this->shapes as $shape)
    {
        $area[] = $shape->area;
    }
    return array_sum($area);
    }
    
    public function output()
    {
        return implode('', array(
            "<h1>",
                "Suma de todas las areas: ",
                $this->sum(),
            "</h1>"
        ));
    }
}
$shapes = array (
    new Circle(3),
    new Square(4)
);

$areas = new AreaCalculator($shapes);

echo $areas->output();

?>