<html>
<head>
	<title>Pruebas</title>
</head>
<body>
	<?php
		class Cube implements ShapeInterface{
			public $lado;
			public function __construct($lado){
				$this->lado=$lado;
			}
			public function area(){
				return ($this->lado * $this->lado)*6;

			}

		}
		class Square implements ShapeInterface{
			public $ladoLongitud;
			public function __construct($ladoLongitud){
				$this->ladoLongitud=$ladoLongitud;
			}
		    public function area()
		    {
        		return pow($this->ladoLongitud, 2);
    		}
		}
		class Circle implements ShapeInterface{
			public $radio;
			public function __construct($radio){
				$this->radio=$radio;
			}
			public function area()
		    {
        		return pi() * pow($this->radio, 2);
    		}

		}
		class AreaCalculator{
			public $figuras;			
			public function AreaCalculator(Array $figurasx){
				$this->figuras = $figurasx;			
			}

			public function sum()
			{				
			    foreach($this->figuras as $figurax)
			    {
			    	if($figurax instanceof ShapeInterface){
				    	//var_dump($figurax);
				        $area[] = $figurax->area();
			    
					}
				}
			    return array_sum($area);
			}
		}

		class VolumenCalculator extends AreaCalculator
		{
			
		}

		class SumCalculatorOutputter {
		    protected $calculator;
		    public function __construct(AreaCalculator $calculator)
		    {
		        $this->calculator = $calculator;
		    }
		    /*public function toJson()
		    {
		        $data = array (
		          'sum' => $this->calculator->sum()
		        );
		        return json_encode($data);
		    }*/
		    public function toHtml()
		    {
		        return implode('', array(
		            '<h1>',
		                'Suma de las areas de las figuras: ',
		                $this->calculator->sum(),
		            '</h1>'
		        ));
		    }
		}
		//////////////////////////////////////////////////////
		interface ShapeInterface {
			public function area();
		}
		//////////////////////////////////IMPLEMENTADO////MAIN...
		$figuras=array(
			new Cube(1),
			new Circle(3),
			new Square(4)
		);
		//var_dump($figuras);
		//echo "<hr>";		
		$area = new AreaCalculator($figuras);
		/*
		var_dump($area->figuras[0]->area());
		echo "<hr>";
		var_dump($area->figuras[1]);
		var_dump($area->figuras[1]->area());
		echo "<hr>";
		*/
		$salida = new SumCalculatorOutputter($area);
		//echo $salida->toJson();	
		echo $salida->toHtml();
		//echo $salida->;
	?>
</body>
</html>